package com.example.vaivui


import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView


import androidx.fragment.app.Fragment




class Fragment1 : Fragment(R.layout.activity_fragment1){

    private lateinit var editTextNote: EditText
    private lateinit var button: Button
    private lateinit var textView: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button = view.findViewById(R.id.button)
        editTextNote = view.findViewById(R.id.editTextNote)
        textView = view.findViewById(R.id.textView)

        val sharedPreferences = requireActivity().getSharedPreferences("MY_NOTES_PREF", Context.MODE_PRIVATE)
        val text = sharedPreferences.getString("NOTE", "")
        textView.text = text


        button.setOnClickListener {
            val note = editTextNote.text.toString()
            val text = textView.text.toString()

            val result = note + "\n" + text

            textView.text = result

            sharedPreferences.edit()
                .putString("NOTE", result)
                .apply()
        }
    }
}
